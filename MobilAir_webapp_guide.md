﻿[![MobilAir logo][logo proj]](https://mobilair.univ-grenoble-alpes.fr/mobilair/)  
_<sup>...pose des défis scientifiques, **impliquant le développement d'une approche inverse consistant à partir d'un objectif formulé en termes d'amélioration de la santé (par exemple, une réduction de 20% de la mortalité liée aux particules) et à identifier ensuite des mesures politiques permettant d'atteindre un tel objectif de santé. Une telle approche serait d'une grande pertinence pour la prise de décision.**</sup>_


# Outil de gestion |_Guide_

\_\_**Préambule** |[_Readme_](#readme)  
\_\_\_\_Fonctionnalités |[_de l'outil_](#liste-des-fonctionnalités-de-loutil)  
\_\_\_\_Types |[_des utilisateurs de l'outil_](#types-des-utilisateurs-de-loutil)  
\_\_\_\_\_\_[_Enquêteur_](#enquêteur)  
\_\_\_\_\_\_[_Gestionnaire_](#gestionnaire)  
\_\_\_\_\_\_[_Informaticien_](#informaticien)

\_\_**Navigation** |[_et organisation des pages_](#navigation-et-organisation-des-pages)  
\_\_\_\_\_\_Recherche |[_par code-barre_](#recherche-par-code-barre)

\_\_**Gestion** |[_des ressources et des plannings_](#gestion-des-ressources-et-des-plannings) _aperçu général et détaillé des acteurs du système_  
\_\_\_\_**Éléments** |[_de page & orientation_](#éléments-de-page-orientation)  
\_\_\_\_\_\_[_Sélection rapide_](#sélection-rapide) [<sup>**fonctionnalités**</sup>](#sélection-rapide-planning)  
\_\_\_\_\_\_\_affichage initial  
\_\_\_\_\_\_\_navigation  
\_\_\_\_\_\_[_Alertes_](#alertes) [<sup>**fonctionnalités**</sup>](#alertes-planning)  
\_\_\_\_\_\_\_Pourquoi les alertes  
\_\_\_\_\_\_\_[_Types et périodes_](#types-et-périodes-des-alertes)  
\_\_\_\_\_\_**Détails** |[_des équipements_](#détails-des-équipements) [<sup>**fonctionnalités**</sup>](#aperçu-matériel-planning-équipement)  
\_\_\_\_\_\_**Équipements** |[_en quelques chifres_](#équipements-en-quelques-chiffres) [<sup>**fonctionnalités**</sup>](#situation-générale-statistiques-planning)  
\_\_\_\_\_\_\_description et comportement d'affichage  
\_\_\_\_\_\_**Plage de planifications** |[_Plannings_](#plage-de-planifications-plannings)  
\_\_\_\_\_\_\_**Réservations** [<sup>**fonctionnalités**</sup>](#réservations-évènements-planning)  
\_\_\_\_\_\_\_\_\_[_Parcours prévisionnel_](#réservations-parcours-prévisionnel) |créées par séquenceur  
\_\_\_\_\_\_\_\_\_Supplémentaires |[_hors parcours habituel_](#réservations-hors-parcours-habituel)  
\_\_\_\_\_\_\_\_\_Statuts |[_des réservations et leurs évolutions_](#statuts-des-réservations-et-leurs-évolutions)  
\_\_\_\_\_\_\_\_\_[_Modifications_](#modifications)  
\_\_\_\_\_\_\_\_\_[_Interprétation des créneaux réservés_](#interprétation-des-créneaux-réservés)  

\_\_**Gestion** |[_flottes - équipements - matériels_](#gestion-des-équipements)  [<sup>**fonctionnalités**</sup>](#gestion-équipement)  
\_\_\_\_**Condition de matériel** |[_statut/état_](#condition-de-matériel-statut-état)  

\_\_**Gestion** |[_utilisateurs_](#utilisateurs)  

\_\_**Gestion** |[_actions - évènements_](#actions)  

\_\_**Questionnaires** |[_questionnaires_](#questionnaires)  

\_\_**Gestion** |[*cohortes*](#cohortes)

\_\_**Annexe**

## Readme

Ce guide contient plusieurs formes de représentation pour fournir une recherche des fonctionnalités de l'outil ciblée et rapide. La partie texte est enrichie avec des illustrations, des tutoriels, des tableaux d'interprétations d'indicateurs d'outil, des tableaux de comportements d'interface utilisateur. Une [syntaxe](#annexe-syntaxe) est proposée pour formaliser l'utilisation des fonctionnalités. **Cela permet de représenter une fonctionnalité clic par clic.**

L'exemple ci-dessous illustre la fonctionnalité - _affichage des détails d'un matériel_. Toutes les fonctionnalités sont représentées de manière similaire pour transmettre synthétiquement le comportement de l'outil.

<kbd>`Sélection rapide`</kbd>:<kbd>`Déplier / Plier`</kbd> -> <kbd>**Équipements**</kbd> -> <kbd>\<catégorie\></kbd> -> <kbd>\<matériel\></kbd> -> <kbd>`Matériel`\<détails\></kbd>

![Contexte sélection rapide][tree howto]
<sup>_Exemples de consultation de la sélection rapide_</sup>

- Les objets en fond opaque, eg. <kbd>`Constant`</kbd>, ont des noms prédéfinis et appropriés dans chaque page d'outil. Cette nomenclature interne ne change pas en général ou elle peut éventuellement changer rarement, avec l'évolution de l'outil.
- Les éléments entre chevrons, eg. <kbd>\<colonne\></kbd>, sont des variables, elles représentent un groupe d'éléments ou un choix arbitraire.
- Les éléments en gras, eg. <kbd>**connue**</kbd>, représentent l'information exacte ou un objet connu en avance - une cible de recherche.

Chaque fonctionnalité illustrée via cette syntaxe est balisée et précédée par un titre en format spécifique : _**`[nom fonctionalité]`**_ contenant une ancre, référencée par son lien interne, retrouvable dans la [section des fonctionnalités](#liste-des-fonctionnalités-de-loutil) ci-dessous.

> Cette approche permet de corriger le guide, de construire de nouvelles interprétations, de recréer une version personnalisée ou simplement un mémo rapide.

### Liste des fonctionnalités de l'outil

Les fonctionnalités sont groupées selon les éléments de l'interface de chaque page de l'application web.

- #### sélection rapide <sup>_`Planning`_</sup>
fonctionnalité | description
-|-
[`[sélection rapide navigation niveaux]`](#sélection-rapide-navigation-niveaux) | _naviguer dans l'arborescence de la sélection rapide_
[`[déplier/plier arborescence]`](#déplierplier-arborescence) | _déplier ou plier l'arborescence de la sélection rapide_)

- #### alertes <sup>_`Planning`_</sup>

fonctionnalité | description
-|-
[`[affichage mes alertes]`](#affichage-mes-alertes) | _afficher alertes enquêteur courant_
[`[affichage toutes les alertes]`](#affichage-toutes-les-alertes) | _afficher alertes totalité des enquêteurs_
[`[filtre alertes (par équipement)]`](#filtre-alertes-par-équipement) | _filtrer alertes équipements uniquement_
[`[affichage plannings ressource à partir de son alerte]`](#affichage-plannings-ressource-à-partir-de-son-alerte) | _afficher plannings d'une ressource (matériel, utilisateur)_
[`[affichage plannings ressource - agrégé]`](#affichage-plannings-ressource-agrégé) | _afficher planning** en forme agrégée_

>>>> filtrage par enqueteur ou + détaillé ou décrire un exemple

- #### aperçu matériel <sup>_`Planning / Équipement`_</sup>

fonctionnalité | description
-|-
[`[édition détails matériel depuis sa réservation]`](#édition-détails-matériel-depuis-sa-réservation) | _basculer vers édition matériel depuis sa réservation_
[`[édition détails matériel depuis page planning I]`](#édition-détails-matériel-depuis-page-planning-i) | _basculer vers édition matériel depuis détails matériel_
[`[édition détails matériel depuis page planning II]`](#édition-détails-matériel-depuis-page-planning-ii) | _basculer vers édition matériel depuis champ code-barre_

- #### situation générale - statistiques <sup>_`Planning`_</sup>

fonctionnalité | description
-|-
[`[équipements vue globale (stats)]`](#équipements-vue-globale-stats) | _bilan des équipements (de la cohorte) en quelques chiffres_
[`[équipements vue globale - par catégorie]`](#équipements-vue-globale-par-catégorie) | _bilan des équipements filtré par catégorie_
[`[équipements vue globale - par statut]`](#équipements-vue-globale-par-statut) | _bilan des équipements filtré par statut_

- #### réservations / évènements <sup>_`Planning`_</sup>

fonctionnalité | description
-|-
[`[création évènement]`](#création-évènement) | _..._
[`[création évènements session - Séquenceur]`](#création-évènements-session-séquenceur) | _..._
[`[modification évènement]`](#modification-évènement) | _..._
[`[modification évènements recalcul - Séquenceur]`](#modification-évènements-recalcul-séquenceur) | _..._

- #### gestion <sup>_`Équipement`_</sup>

fonctionnalité | description
-|-
[`[ajout matériel]`](#ajout-matériel) | _..._
[`[gestion de statut matériel 'En acquisition']`](#gestion-de-statut-matériel-en-acquisition) | _..._

> - #### exemples <sup>_`(guide d'utilisation)`_</sup>
> fonctionnalité | description
> -|-
> [`[syntaxe nomenclature exemples]`](#syntaxe-nomenclature-exemples) | _exemples d'illustrations des fonctionnalités de l'outil_

### Types des utilisateurs de l'outil

- #### _Enquêteur_

C'est l'utilisateur _`alpha`_ de l'outil. Il est responsable de la gestion des [_**planifications**_](#gestion-des-ressources-et-des-plannings) des _**participants**_ et le déroulement des _**sessions**_. Il peut partager ses tâches avec d'autres enquêteurs et il peut modifier la condition d'un équipement.

_`gestion : planifications * sessions`_

- #### _Gestionnaire_
C'est l'utilisateur _`intégrateur - administrateur de l'étude`_. Il est responsable de la saisie des [utilisateurs](#utilisateurs) (_**enquêteurs**_, _**participants**_) ainsi que du [_**matériel**_](#gestion-des-équipements). Aussi, il prépare les _messages, l'envoi des _**messages**_ et il programme les _**évènements**_ / [_**actions**_](#actions) (contexte, durée). Tous ces _'éléments-acteurs'_ sont nécessaires pour _composer_ les _**sessions**_.

_`pré-remplissage : utilisateurs * matériel * messages * évènements`_

- #### _Informaticien_

L'informaticien utilise l'outil d'une autre manière, il n'intervient pas dans la gestion. Ses activités sont la vérification et les tests des fonctionnalités de l'outil et l'intervention en cas de disfonctionnement. Il est _`administrateur de l'outil` (admin)_ et il semble inactif pour les gestionnaires et les enquêteurs. Parmi tous les utilisateurs il a le seul le pouvoir de gérer les _**droits d'utilisateurs**_ et les _**cohortes**_.

_`gestion : droits * cohortes; intervention`_

Le contenu de ce guide est organisé en deux parties, dédiées aux deux utilisateurs principaux (enquêteur, gestionnaire). Voir [_participants_](#utilisateurs).

## Navigation et organisation des pages

La navigation entre les pages de l'application se fait via le menu en haut toujours visible. Les fonctionnalités sont regroupées en trois groupes selon le contexte d'utilisation.

#### Organisation de la navigation

La section _`authentification`_ contient les fonctionnalités de base (connexion, mot de passe,...) utilisées par tous les utilisateurs, la section _`enquêteurs`_ contient les pages enquêteur, la section _`gestionnaires`_ contient les pages gestionnaires. 

##### I section Authentification

![Navigation authentification][navig auth]  
<sup>Illustration : _Accès aux fonctionnalités générales utilisateurs : authentification et gestion congés_</sup>

Permet d’accéder aux fonctionnalités de :
- identification - connexion
- modification de mot de passe
- gestion des congés

##### II section Enquêteurs

![Navigation enquêteur][navig enquet]  
<sup>Illustration : _Accès aux pages enquêteurs : planification, gestion des utilisateurs_</sup>

Permet de gérer les :
- planifications des ressources _(gestion des réservations matériel, participants)_
- planifications des sessions _(aspect temporel)_

##### III section Gestionnaires

![Navigation gestionnaire][navig gestion]  
<sup>Illustration : _Accès aux pages gestionnaires_</sup>

Permet d’accéder à la gestion de :
- matériels
- évènements _(organisation des tâches des sessions)_
- questionnaires
- ~~cohortes~~

> Les gestionnaires peuvent gérer également le contenu enquêteur.

![Navigation présentation complète][navig overall howto]  
<sup>Illustration : _Naviguer vers les pages de l'application Mobil'Air_</sup>

#### Recherche par code-barre

Le champ code-barre permet de saisir un numéro de série à la main ou par scan pour rechercher les détails de ressource consulté. De plus son input est effectué de manière dynamique en cliquant sur un agenda.

## Gestion des ressources et des plannings

C'est la page principale de l'application - un tableau de bord centralisé avec un contrôle sur :
- les évènements, les actions
- les matériels et réservations correspondantes
- les utilisateurs, leurs rendez-vous répartis selon les durées des sessions, le matériel utilisé sur des périodes de temps

La page permet de suivre les flottes des matériels, leurs états/statuts, les utilisateurs, leurs actions (évènements, sessions), leurs disponibilités, la situation actuelle des dispositifs engagés pendant la mission du projet.

C'est une interface pour consulter l'information de façon détaillée ou générale le plus vite possible et ensuite réagir, organiser ou réorganiser le travail en cours.

### Éléments de page & orientation

L'organisation de cette page contient les éléments suivants :
- [sélection rapide](#sélection-rapide) _=> visualisation de la planification des ressources, des éléments et détails spécifiques_
- [planning](#plage-de-planifications-plannings) _=> gestion de réservations rendez-vous et matériel_
- table [alertes](#alertes) _=> liste des 'urgences' matériels ou rendez-vous_
- [détails](#détails-des-équipements) des équipements _=> affichage détails matériel_
- [statistiques](#équipements-en-quelques-chiffres) _=> aperçu de la situation des matériels d'une cohorte en quelques chiffres_

Ces éléments s'affichent selon le contexte d'utilisation de l'utilisateur connecté (certains dépendent des droits utilisateur).

### Sélection rapide

La structure représentée par une liste hiérarchique ou arborescence est la `Sélection rapide`. Le terme **niveau** est utilisé pour situer les objets dans l'arborescence. Toutes les recherches d'information sont initiées à partir de cet endroit.

L'utilisateur peut trouver les ressources (flotte / catégories / matériels / personnel) de la cohorte à laquelle il appartient en quelques clics.

    / Tout                              racine - niveau (0)
	  |----- Cohorte A                                  (1)
	  |      |----- Enquêteurs                          (2)
	  |      |      |----- Enquêteur 1                  (3)
	  |      |      |----- Enquêteur 2	  
	  |      |      +
	  |      |
	  |      |----- Participants                        (2)
	  |      |      |----- Participant 1                (3)
	  |      |      |----- Participant 2	  
	  |      |      +
	  |      |	  
	  |      |----- Équipements                         (2)
	  |             |----- Catégorie 1                  (3)
	  |             |      |----- Matériel 11           (4)
	  |             |      |----- Matériel 12
	  |             |      +
	  |             |
	  |             |----- Catégorie 2                  (3)
	  |             |      |----- Matériel 21           (4)	  
	  |             |      +
	  |             |	  
	  |             +	  	  
	  |
	  |----- Cohorte B                                  (1)
      +

<sup>Illustration : _Structure de la sélection rapide_</sup>

Ci-dessous quelques scénarii de navigation en texte et image:  

##### _`[sélection rapide navigation niveaux]`_
<kbd>**Tout _(0)_**</kbd> -> <kbd>\<cohorte\> **_(1)_**</kbd> -> <kbd>\<personnel\> **Enquêteurs _(2)_**</kbd> -> <kbd>\<enquêteur connecté\> **_(3)_**</kbd>  
<kbd>**Tout _(0)_**</kbd> -> <kbd>\<cohorte\> **_(1)_**</kbd> -> <kbd>\<flotte\> **Équipements _(2)_**</kbd> -> <kbd>\<catégorie\> **_(3)_**</kbd> -> <kbd>\<matériel\> **_(4)_**</kbd>  
<sub>**Remarque :** _En gras - les noms de niveaux actuellement **codés en dur** dans l'application, les autres groupes sont définis par les utilisateurs (enquêteurs ou gestionnaires)_ - voir [exemple](#readme) ou [syntaxe et nomenclature](#syntaxe-nomenclature-exemples)</sub>

![Sélection rapide][tree levels]  
<sup>Illustration : _Niveaux de la sélection rapide_</sup>

Le clic sur un niveau invoque l'affichage des [éléments d'interface](#éléments-de-page-orientation) correspondants. Exemples de consultation de la sélection rapide par niveaux et information correspondante affichée :

clic (niveau) | statuts/états _(stats)_ de | plannings de | alertes concernant | détails de
-|-|-|-|-
<kbd>**Tout _(0)_**</kbd> | [x]<sup>**`matériel`** par catégorie pour toutes les cohortes</sup> | [ ] | [ ] | [ ]
<kbd>\<cohorte\> **_(1)_**</kbd> | [x]<sup>**`matériel`** par catégorie pour la cohorte de l'utilisateur connecté</sup> | [x]<sup>tous **`acteurs`** & **`équipements`** de la cohorte</sup> | [ ] | [ ]
<kbd>\<personnel\> **_(2)_**</kbd> | [ ] | [x]<sup>tous **`acteurs`**</sup> | [x]<sup>tous **`acteurs`**</sup>
<kbd>\<enquêteur\> **_(3)_**</kbd> | [ ] | [x]<sup>**`enquêteur` sélectionné</sup> | [x]<sup>les actions de l'**`enquêteur`**</sup>
<kbd>\<participant\> **_(3)_**</kbd> | [ ] | [x]<sup>**`participant`** sélectionné & **`enquêteur`** connecté**</sup> | [x]<sup>les actions du **`participant`**</sup>
<kbd>**Équipements _(2)_**</kbd> | [ ] | [x]<sup>**`équipements`** par cohorte</sup> | [x]<sup>**`équipements`** par cohorte</sup>
<kbd>\<catégorie\> **_(3)_**</kbd> | [ ] | [x]<sup>**`équipements`** par catégorie</sup> | [x]<sup>**`équipements`** par catégorie</sup>
<kbd>\<matériel\> **_(4)_**</kbd> | [ ] | [x]<sup>**`équipement`** sélectionné</sup> | [ ] | <sup>**`équipement`** sélectionné</sup>

<sup>Tableau : _Types d'information affichés par niveau_</sup>

![Contexte sélection rapide][tree howto]  
<sup>Illustration : _Obtenir l'information selon le contexte de la sélection rapide_</sup>

L'affichage des réservations est décrite dans la section [planification](#plage-de-planifications-plannings) du document.

### Alertes

Les alertes sont des rappels sur les évènement planifiés, rectifiés dans le tableau des alertes pour rendre l'organisation d'une session plus efficace. Les actions prévues et consultables dans le planning sont alors affichées en avance selon une marge temporelle prédéfinie.

#### Pourquoi les alertes

La table des alertes est interactive. Le clic sur une alerte permet de distinguer la ressource en alerte dans la liste de sélection rapide, ce qui permet de visualiser son information détaillée plus rapidement. Son planning est également affiché en même temps.

#### Types et périodes des alertes

Les alertes sont actualisées **dynamiquement** après chaque opération utilisateur - interaction sur les éléments de la page : _`sélection rapide`, `alertes`, `création/modification de réservation`_. La liste des alertes est construite en fonction des paramètres suivants :
- durée entre la date du jour et la date d'évènement _(en jours)_
- importance _(urgence, par exemple la date d'échéance se rapproche)_
- confirmation de rendez-vous impérative
- statut _(prévu, projeté par séquenceur))_

Pendant une session enquêteur la liste est susceptible de changer sans l'intervention de l'enquêteur si les alertes activées s'approchent de la date du jour.

Les alertes sont représentées par 3 couleurs _(vert, orange, rouge)_ et classées en deux groupes selon l'urgence.

code couleur | urgence
:-:|:-:|
![#99FF99][pstgreen] | information
![#FF9999][pstred] ![#99FF99][pstorange] | alerte à résoudre

<sup>Tableau: _Classement des alertes_</sup>

Ci-dessous deux tableaux d'alertes regroupés par rôle de réservation - voir [_interprétation des créneaux réservés_](#interprétation-des-créneaux-réservés). Les rendez-vous confirmés sont ceux confirmés par les deux acteurs d'une action _(enquêteur, participant)_ - voir [_statuts des réservations et leurs évolutions_](#statuts-des-réservations-et-leurs-évolutions).

code couleur | période d'affichage | quand | confirmé | statut évènement | action
:-:|:-:|-|:-:|-|-
![#99FF99][pstgreen] | 1 jour | le jour du RDV | [x] | prévu ou calculé | rendez-vous aujourd'hui
![#99FF99][pstgreen] | 1 jour | le jour du RDV | [ ] | prévu ou calculé | rendez-vous aujourd'hui
![#99FF99][pstgreen] | 7 à 3 jour | avant le RDV |  [ ] | prévu ou calculé | rendez-vous à confirmer
![#99FF99][pstorange] | 3 jours | précédents du RDV |  [ ] | prévu ou calculé | rendez-vous à confirmer
![#99FF99][pstgreen] | 1 jour | le jour de notification |  [ ] || notification à traiter
![#99FF99][pstorange] | sans limite | à partir de lendemain de la notification |  [ ] || notification à traiter

<sup>Tableau: _Règles d'affichage des alertes enquêteur / participant_</sup>

code couleur | période d'affichage | actuellement réservé | état matériel | statut matériel
:-:|:-:|:-:|-|-
![#99FF99][pstorange] | sans limite | [ ] || à décharger
![#FF9999][pstred] | <sup>**si nouvelle réservation dans moins de**</sup> 3 jours | [x] || à décharger
![#99FF99][pstorange] | sans limite | [ ] | en panne
![#FF9999][pstred] | sans limite | [x] | en panne

<sup>Tableau: _Règles d'affichage des alertes matériel_</sup>

#### Fonctionnalités des alertes

Les alertes de l'enquêteur connecté s'affichent:  

- à chaque chargement de page
- au clic sur l'enquêteur connecté ~ 3ème niveau de la [sélection rapide](#sélection-rapide):
  ##### _`[affichage mes alertes]`_
  <kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Enquêteurs**</kbd> -> <kbd>\<enquêteur connecté\></kbd> -> <kbd>`Alertes`\<enquêteur connecté\></kbd>

Toutes les alertes s'affichent:  
- clic sur le 2ème niveau (personnel, matériel):
  ##### _`[affichage toutes les alertes]`_
  <kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Enquêteurs<br>Gestionnaires<br>Informaticiens<br>Participants<br>Équipements**</kbd> -> <kbd>`Alertes`</kbd>

- clic sur la catégorie ~ 3ème niveau (matériel):

<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Équipements**</kbd> -> <kbd>\<catégorie\></kbd> -> <kbd>`Alertes`</kbd>

Pour filtrer les alertes `Équipement`:
##### _`[filtre alertes (par équipement)]`_
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Équipements**</kbd>  
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Équipements**</kbd> -> <kbd>\<catégorie\></kbd>

##### _`[affichage plannings ressource à partir de son alerte]`_
<kbd>`Alertes`</kbd>:<kbd>\<ligneAlertes\></kbd> -> <kbd>`Sélection rapide`</kbd>:<kbd>\<ressource\></kbd>  -> <kbd>`Plannings`</kbd>:<kbd>\<ressource\></kbd>

> Le clic sur un nœud de l'arborescence - `Sélection rapide` affiche toujours les `Plannings` de multiples agendas - _\<ressource\>_. La sélection d'`Alertes` permet ensuite de réduire les planifications sur deux agendas (enquêteur et participant) ou plus pour sélectionner les évènements à traiter.  
> La visualisation d'une planification d'un agenda (si ses évènements existent), est possible depuis une feuille de l'arborescence [<sup>_niveaux (3),(4)_</sup>](#sélection-rapide-navigation-niveaux).

![Mécanisme alertes][alert mechanics howto]  
<sup>Illustration : _Mécanisme des alertes, trouver les sources d'alertes, les vérifier et interagir_</sup>

Les étapes pour afficher un évènement à traiter :

##### _`[affichage plannings ressource - agrégé]`_
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>**Enquêteurs<br>Gestionnaires<br>Informaticiens<br>Participants<br>Équipements**</kbd> -> <kbd>`Alertes`</kbd>:<kbd>\<ressourceAlerte\></kbd> -> <kbd>`Plannings`</kbd>:<kbd>\<ressource\></kbd> - > <kbd><réservation></kbd>

Pour accéder directement à un agenda/ressource non disponible sur la liste d'alertes (pas d'alerte sur l'agenda) sans descendre niveau par niveau, il est possible de déplier l'arborescence.

##### _`[déplier/plier arborescence]`_
<kbd>`Sélection rapide`</kbd>:<kbd>`déplier/plier`</kbd> -> <kbd>\<ressource\></kbd> -> <kbd>`Plannings`</kbd>:<kbd>\<ressource\></kbd>

<kbd>`Sélection rapide`</kbd>:<kbd>`déplier/plier`</kbd> -> <kbd>\<ressource\></kbd> -> <kbd><détails\></kbd>

### Détails des équipements

L'aperçu de l'information détaillée depuis la page `Gestion planification` permet de:
- vérifier le matériel en consultation
- modifier ses caractéristiques dans la page `Gestion des équipements` _- voir [statuts/états](#condition-de-matériel-statut-état) dans la section matériel_.

![Ressource détail][details howto]  
<sup>Illustration : _Afficher l'information spécifique à une ressource (matériel, utilisateur) de la cohorte_</sup>

Le clic sur _l'information détaillée_ d'un matériel invoque la fiche de modification de la page [Gestion de matériel](#gestion-des-équipements).

##### _`[édition détails matériel depuis sa réservation]`_
<kbd>`Gestion plannification`</kbd>:<kbd>`Sélection rapide`</kbd>:<kbd>**déplier/plier**</kbd> -> <kbd>\<matériel\></kbd> -> <kbd>`Matériel`\<détails\></kbd> -> <kbd>`Gestion de matériel`</kbd>:<kbd>`Matériel`\<détails\></kbd>  
<sub>**Remarque:** _Noter la différence entre les objets `Matériel`\<détails\> de la page `Gestion planification` et `Gestion de matériel`_</sub>

Accessible aussi par un autre enchainement :
##### _`[édition détails matériel depuis page planning I]`_
<kbd>`Gestion plannification`</kbd>:<kbd>`Plannings`</kbd>:<kbd>\<ressource\></kbd> -> <kbd>\<évènement\></kbd> -> <kbd>**Modification**\<évènement\>\<matériel\></kbd>:<kbd>**Q** (lien Loupe)</kbd> ->  <kbd>`Gestion de matériel`</kbd>:<kbd>`Matériel`\<détails\></kbd>

##### _`[édition détails matériel depuis page planning II]`_ 
<kbd>pageArbitraire</kbd>:<kbd>**input**\<code-barre\></kbd>:<kbd>**||ll|l|** (lien code-barre)</kbd> ->  <kbd>`Gestion de matériel`</kbd>:<kbd>`Matériel`\<détails\></kbd>

Le champ d'entrée [code-barre](#recherche-par-code-barre) est localisé au dessous de la [navigation](#navigation-et-organisation-des-pages) principale.

### Équipements en quelques chiffres

Les _'statistiques'_ contiennent l'information sur le nombre de matériels dans une catégorie répartis par leurs `statuts` et `états`.

![Statistique cohorte][stats howto]  
<sup>Illustration : _Aperçu de la situation des ressources dans une ou plusieurs cohortes_</sup>

L'information sur les états des équipements et leurs statuts actuels est construite dans le tableau qui s'affiche via la sélection rapide.

##### _`[équipements vue globale (stats)]`_
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>`Catégorie/Statut aperçu - Ensemble`</kbd> <sup>_(en connexion admin)_</sup>  
ou  
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>`Catégorie / Statut aperçu`\<cohorte\></kbd> 

Les résultats peuvent être filtrés par catégorie:
##### _`[équipements vue globale - par catégorie]`_
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>`Catégorie / Statut aperçu`\<cohorte\></kbd>:<kbd>**Filtrage**</kbd>:<kbd>`par catégorie`</kbd> 

Ou par deux des statuts spécifiques:
##### _`[équipements vue globale - par statut]`_
<kbd>`Sélection rapide`</kbd>:<kbd>**Tout**</kbd> -> <kbd>\<cohorte\></kbd> -> <kbd>`Catégorie / Statut aperçu`\<cohorte\></kbd>:<kbd>**Filtrage**</kbd>:<kbd>`par statut`</kbd> -> <kbd>**En acquisition**<br>**À décharger**</kbd>

### Plage de planifications - Plannings

La planification s'effectue par un mécanisme de gestion qui permet de gérer des réservations : de manière `automatique - projetées par le séquenceur`,`hors parcours habituel` et par `modifications`.  Un enquêteur mène les sessions pour plusieurs participants et son planning doit correspondre à tous les évènements - actions de chaque session. Cela demande un enquêteur organisé et rigoureux ainsi qu'une gamme de fonctionnalités de planification fournies par l'application.

Pour construire cette organisation de plannings complexe inter-croisant les activités de tous les acteurs, les fonctionnalités comprennent :

- réservations supplémentaires (hors parcours) - pour un rendez-vous
- réservations automatiques pour établir une plage de réservations de sessions
- déplacement de réservation
- recalcul de session
  - en déplaçant les dates de session concernée ou
  - en déplaçant tous les évènements (actions/sessions) dépendants à l'action/session concernée
 - transmission de réservation à un autre enquêteur _(remplacement d'un enquêteur)_
 - remplacement d'un matériel par un autre pour une réservation

##### dépendance entre les sessions

#### Réservations parcours prévisionnel

Les réservations prévisionnelles d'un participant sont les évènements ([_de type RDV_](#interprétation-des-créneaux-réservés)), planifiés de manière automatique pour la durée d'une session ou pour la totalité des sessions - _**parcours type calculé**_. Cette planification est calculée ou projetée par le séquenceur - l’algorithme de l'outil qui prend pour paramètres les disponibilités du participant concerné, celles de son enquêteur et des matériels qui lui seront dédiés au cours des sessions. Chaque action incluse dans le calcul a ses propres [critères et limites temporelles](#actions) ce qui augmente la complexité de construction du planning. La création des évènements par ce processus est lancée en quelques clics.

![Planning nouveau RDV][event new howto]
<sup>Illustration : _Créer une réservation RDV_</sup>

Dans certains cas (réservations en cours, indisponibilités, délais non acceptables, ...) il sera impossible d'obtenir une planification. L’enquêteur peux relancer l’opération et selon la situation :
- modifier la date de début
- dédier au participant un autre enquêteur pour un ou plusieurs évènements (ou sessions)?
- ...

##### _`[création évènements session - Séquenceur]`_

#### Statuts des réservations et leurs évolutions

Les évènements d'un planning _automatique_ ont un statut **calculé** par défaut. À ce stade il s'agit d'un planning _prévisionnel_ et d'évènements prévisionnels. Les planifications de longue durées pour plusieurs participants permettent à l'enquêteur de créer une vision élargie sur la chronologie de ses activités.

Un évènement __prévu__ a été replanifié / modifié selon les modalités d'un parcours - changement des disponibilités à cause de maladie, congés, réorganisation. Le premier rendez-vous de chaque session a aussi le statut 'prévu', car les créneaux des évènements sont _`convenu`_ en avance entre le participant et l'enquêteur. Ils sont _ajustés_ mais pas encore confirmés.

Une fois les disponibilités confirmées par l’enquêteur et le participant l'évènement peut être __confirmé__.

La réalisation d'un évènement est alors vérifiée pendant les étapes  :

_`calcul * prévision * confirmation`_

> Les évènements _confirmés_ et _prévus_ **sont exclus** des calculs automatisés.**

![Réservation séquenceur][res auto howto]
<sup>Illustration : _Créer une réservation via séquenceur_</sup>

#### Réservations hors parcours habituel

Les réservations supplémentaires sont utilisés pour planifier des rendez-vous imprévus pendant une session en cours et sans recalcul. Une réservation hors parcours peut s'ajouter à une séquence complète d'une session dans certains cas :

- décalage d'un RDV (avec suppression et création)
- création d'un RDV pour remplacer un équipement
- transmission des évènements au un autre enquêteur

![Réservation supplémentaire][res extra howto]
<sup>Illustration : _Créer une réservation extra_</sup>

La réservation ne déclenche pas le calcul de planification quand le champ _`RDV concerné`_ reste vide (aucune valeur est choisie).

##### _`[création évènement]`_
<kbd>`Plannings`</kbd>:<kbd>\<ressource\></kbd> -> <kbd>\<créneau_inoccupé\></kbd> -> 

#### Interprétation des créneaux réservés

Le planning des réservations contient les **évènements de type rendez-vous** - _RDV_ et les **actions de type notification**. Les notifications sont les remarques qui servent aux enquêteurs pour la préparation des évènements et elles s'affichent dans le planning sous la forme d'un rendez-vous spécifique _(durée - 15 min)_ le jour où une action est à réaliser.

Une réservation matériel est un RDV de type _'relation matériel-participant'_ contrairement à un RDV de type _'relation enquêteur-participant'_. Sur le tableau des réservations les deux ont les mêmes caractéristiques d'affichage.

Les éléments du planning ont leurs propres codes couleurs pour distinguer le statut d'une réservation.

code couleur | statut | type évènement
:-:|-|-
![#99FF99][pstgreen] | _confirmé_ | rendez-vous
![#9999FF][pstyellow] | _prévu_ | rendez-vous
![#99FF99][pstorange] | _calculé_ | rendez-vous
![#E0E0E0][ltgrey] | _passé_ | rendez-vous
![#9999FF][pstblue] | _non résolue_ | notification
![#E0E0E0][ltgrey] | _résolue_ | notification

<sup>Tableau: _Représentation des statuts des évènements par couleur_</sup>

![Planning aperçu][planning_overview]
<sup>Illustration : _L'aperçu d'élément planning_</sup>

Les statuts donnent également un meilleur aperçu sur la situation du planning.

statut évènement | description
-|-
confirmé | _par l'enquêteur et le participant une semaine avant la date prévue (appel téléphonique)_
prévu | _un premier accord sur le créneau ou RDV reporté car les disponibilités d'un participant ont changé_
calculé | _projeté par séquenceur_

<sup>Tableau: _Significations des statuts_</sup>

#### Modifications

La modification d'un évènement est un déplacement des dates d'une réservation ou son raccourcissement. Par exemple, quand le statut d'un matériel devient 'à décharger' sa réservation se termine.

Une session peut aussi être modifiée, dans ce cas ses évènements sont recalculés grâce au séquenceur.

![Modification réservation][res modif howto]
<sup>Illustration : _Modifier une réservation_</sup>

##### _`[modification évènement]`_

##### _`[modification évènements recalcul - Séquenceur]`_

## Gestion des équipements

L'information de tous les équipements de collecte/production des données utilisés sur le terrain pendant une période ou une durée de session d'un participant est stockée dans _l'application - outil de gestion_.

Identifié avec un numéro de série et d'autres attributs, un équipement peut être enregistré (mis en circulation) et consulté via la page _Gestion des matériels_. Dans ce guide un `équipement` est nommé aussi `matériel` ou `capteur` ou `ressource`.

Un nouvel équipement est ajouté une fois tous ses détails remplis et confirmés dans la fenêtre de saisie.

##### _`[ajout matériel]`_
<kbd>`Matériels`</kbd>:<kbd>`Gestion des équipments`</kbd>:<kbd>**+**</kbd> -> <kbd>`Matériel`\<détails\></kbd> -> <kbd>`Nom matériel`<br>`Catégorie`<br>`code-barres`<br>`numéro de serie`<br>`état`<br>`statut`<br>`journal`</kbd>

![Nouveau matériel][equip new howto] _(option)_
<sup>Ajout d'un matériel</sup>

#### Condition de matériel - statut / état

Les conditions sont prédéfinies et proposées par l'application. Un matériel peut avoir un des états suivants : _`Fonctionnel`_, _`De secours`_, _`Maintenance`_, _`En panne`_, _`Perdu`_. Il a aussi son statut actuel : _`Initialisé`_, _`En acquisition`_, _`À décharger`_; indiquant sont _'état d'opérabilité à l’instant'_.  La combinaison **statut/état** est l'indicateur de fonctionnement et de disponibilité d'un matériel. Elle est gérée par **enquêteur** ou **gestionnaire**. 

condition | type | description
-|-|-
Initialisé | statut | _anciennes données déchargées, configuré / calibré, disponible en stock_
**En acquisition <sup>(1)</sup>** | statut | _en utilisation ~ en opération sur le terrain ~ session de mesure en cours_
A décharger | statut | _revenu au labo après une session de mesure, en attente du déchargement des données, non disponible pour nouvelle utilisation, **visible sur la liste de matériels à réserver**_
Fonctionnel | état | _fonctionne correctement_
De secours | état | _remplacement en cas de dysfonctionnement_
Maintenance | état | _en cours de réparation (au laboratoire ou chez fournisseur, à préciser dans le journal)_
En panne | état | _présent au laboratoire, non disponible en stock car non fonctionnel_
Perdu | état | _perdu, non disponible en stock_

<sup>Tableau: _Liste des conditions - statuts/états des matériels_</sup>

<sup>(1)</sup> En revanche le statut __`En acquisition`__ est calculé par le système. Un matériel est automatiquement _'En acquisition'_ si il y a une réservation qui se déroule à l'instant. Si un matériel est consulté avant la date/temps de début de réservation ou après la date/temps de fin de réservation son statut sera **`Initialisé`**.

L'utilisateur peut intervenir et interrompre le _mode_ _'En acquisition'_ manuellement via la page matériel:
##### _`[gestion de statut matériel 'En acquisition']`_
- si une session de mesure se termine avant la fin de la période prévue  
<kbd>`Gestion des équipments`</kbd><sup>**2x clic !**</sup> :<kbd>\<détails\></kbd> -> <kbd>`Statut`</kbd> -> <kbd>**À décharger**</kbd>

- quand l'_**état**_ du matériel est modifié à **`En panne`** ou à **`Perdu`** à cause d'une casse ou d'un évènement imprévu  
<kbd>`Gestion des équipments`</kbd><sup>**2x clic !**</sup> :<kbd>\<détails\></kbd> -> <kbd>`Etat`</kbd> -> <kbd>**En Panne**<br>**Perdu**</kbd>

![Modification matériel][equip modif howto]
<sup>Illustration : _Modifier un matériel : statut, état_</sup>

## Utilisateurs

La page de gestion des utilisateurs permet de saisir tous les utilisateurs - acteurs dans le système de l'outil. Hormis les utilisateurs de type [_gestionnaires_](#types-des-utilisateurs-de-loutil) les utilisateurs de type `participant` font aussi partie des acteurs. Ils ont un autre rôle, ils constituent un groupe d'utilisateurs gérés/suivis par les gestionnaires/enquêteurs. Un participant est en mission, son parcours est géré de manière chronologique.

Techniquement, les acteurs et les ressources (matériel) sont interprétés par l'outil de la même manière. Un code-barre est attribué à chaque équipement et analogiquement une étiquette est générée pour chaque utilisateur, ce qui simplifie la gestion, la navigation [(recherche des évènements)](#recherche-par-code-barre) et permettra de continuer le traitement des données et les recherches après la mission.

![Création utilisateur][user new howto]
<sup>Illustration : _Gérer un utilisateur_</sup>

## Actions

La page de gestion des actions est disponible par les gestionnaires uniquement et permet de programmer les activités pour les participants d'une cohorte.

![Actions Sessions][actions preview structure]
<sup>Illustration : _Aperçu de gestion des actions (taches)_</sup>

Chaque session contient une suite de tâches - actions prédéfinies avec leurs :
- descriptions
- durées
- délais ou décalage entre les actions et respectivement, entre les sessions
- dates de début - référencées soit par le début de session à laquelle l'action appartient, soit par une action précédente

> L'affichage des actions a une structure spécifique qui est utilisée pour le calcul des réservations. Les paramètres pris en compte sont : nom action, décalage, tolérance et durée.

![Actions Nouvelle Tache][actions new howto]
<sup>Illustration : _Ajouter une nouvelle tache_</sup>

## Questionnaires
## Cohortes

## Annexe - Syntaxe

```xml
<élément d'interface>[<geste>[<page>:]<élément(s) suivant(s)>]
```
```xml
[<page>:]<élément principal>:<sous-élément>[<geste><élément(s) suivant(s)>]
```
##### _`[syntaxe nomenclature exemples]`_
<kbd>`appPageA`</kbd>:<kbd>`element_appPageA`</kbd>:<kbd>\<subElement\></kbd> -> <kbd>`popUpA`</kbd>:<kbd>`knownList`</kbd> -> <kbd>**inputField**</kbd>

<sup>Exemple: _Enchainement des gestes pour localiser un champ de saisie appelant **inputField** à partir de la **pageA** et ses sous-éléments._</sup>

<kbd>`nomTable`</kbd>:<kbd>\<ligneTable\></kbd> -> <kbd>\<détails\></kbd>:<kbd>`nomCombo`</kbd> -> <kbd>**val A<br>val B<br>...<br>val N**</kbd>

<sup>Exemple: _Flux d'utilisation en cliquant une ligne **\<ligneTable\>**, l'élément de la table **nomTable**, affichant un élément avec les **\<détails\>** dont un combo-box nommé **nomCombo** contient les valeurs connues à sélectionner._</sup>


syntaxe | représentation graphique | sa forme dans l'outil
-|-|-
_\<page\>_ | <kbd>page</kbd> | _une page de l'outil <sup>élément optionnel</sup>_
_\<élément principal\>_ | <kbd>élément principal</kbd> | _élément html de base (affiché par défaut et toujours) sur la page courante, par exemple **une table**_
_\<geste\>_ | `->` | _signifie une geste de souris, dans la plupart des cas un **clic**_
_:_ | `:` | _signifie une geste ou 'la combinaison visuelle' entre un élément et son sous-élément sur la page courante ; l'élément **contient** son sous-élément et le deux sont affichés / accessibles_
_\<sous-élément\>_, _\<élément d'interface\>_ | <kbd>sous-élément</kbd>, <kbd>élément d'interface</kbd> | _un composant d'élément principal - ancêtre, par exemple une **ligne de table** ou un **combo-box** dans un **pop-up**_ ou un élément menant vers une autre **page**
_\<élément suivant\>_ | <kbd>élément suivant</kbd> | _**éléments invoqués**/affichés/recherchés par les étapes précédentes_
_\<\>_ | _(aucune)_ | _objet **nécessaire**, par exemple \<une geste\> nécessite \<un élément\> pour l'étape suivante_
_[]_ | _(aucune)_ | _un groupe d'objets **optionnels** - une chaine d'étapes pour progresser vers l'information recherchée ou l'indicateur d'une page (préfixé)_

<sup>Table : _Syntaxe utilisé pour décrire le flux de gestes activant les fonctionnalités d'application_</sup>

Voir [l'utilisation](#readme) de syntaxe.

---
#### _markdown & git references_

<sup>[md anchored links with special characters]  
[MD color workaround]  
[handbook markdown guide]  
[gitlab-foss markdown doc]  
https://docs.gitlab.com/  
[start using git]</sup>

[navig auth]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/menu_context_auth.png "Section authentification"
[navig enquet]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/menu_context_enquet.png "Section enquêteur"
[navig gestion]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/menu_context_gestion.png "Section gestionnaire"
[navig overall howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_guide_navig.gif "Navigation tout détaillé"

[tree levels]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_tree_levels.gif "Monter et descendre l'arborescence"
[tree howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_tree_interact.gif "Consultation des détails spécifiques via la sélection rapide"
[alert mechanics howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_alerts_interact.gif "Affichage et recherche des sources des alertes"
[details howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_details.gif "Détail d'une ressource"
[stats howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_stats.gif "Cohorte/s en quelques chiffres"

[res extra howto]: http://localhost/guide/media/MobilAir_res_extra.gif "Créer une réservation hors parcours habituel"
[res auto howto]: http://localhost/guide/media/MobilAir_res_auto.gif "Créer une réservation grace au séquenceur"
[res modif howto]: http://localhost/guide/media/MobilAir_res_modif.gif "Modifier une réservation"

[equip new howto]: http://localhost/guide/media/MobilAir_equip_new.gif "Ajouter nouveau matériel"
[equip modif howto]: http://localhost/guide/media/MobilAir_equip_modif.gif "Modifier le statut ou l'état d'un matériel"

[user new howto]:  https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/user_new.png "Détails / gestion des utilisateurs"

[event new howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/event_new.png "Réservation rendez-vous"
[planning_overview]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/planning_overview.png "Plage de planifications"

[actions preview structure]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/actions_structure.png "Gestion des actions ~ 'paramètres' du Séquenceur - calcul automatique"
[actions new howto]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/actions_new.png "Créer nouvelle action"

[logo proj]: https://gricad-gitlab.univ-grenoble-alpes.fr/copio/outil-de-gestion-intermob-mobilair/raw/master/media/MobilAir_logo.png "Project logo Mobil'Air"

[md anchored links with special characters]: https://gist.github.com/asabaylus/3071099#gistcomment-3423745 "https://gist.github.com/asabaylus/3071099#gistcomment-3423745"
[MD color workaround]: https://github.com/github/markup/issues/369#issuecomment-654900693 "https://github.com/github/markup/issues/369#issuecomment-654900693"
[handbook markdown guide]: https://about.gitlab.com/handbook/markdown-guide "https://about.gitlab.com/handbook/markdown-guide"
[gitlab-foss markdown doc]: https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md "https://gitlab.com/gitlab-org/gitlab-foss/blob/master/doc/user/markdown.md"
[start using git]: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html "https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html"

[pstgreen]: https://placehold.it/15/99FF99/000000?text=+  "pastel green #99FF99"
[pstyellow]: https://placehold.it/15/FFFF99/000000?text=+ "pastel orange #FFFF99"
[pstorange]: https://placehold.it/15/FFCC33/000000?text=+ "pastel orange #FFCC33"
[pstred]: https://placehold.it/15/FF9999/000000?text=+ "pastel red #FF9999"
[darkpink]: https://placehold.it/15/FF6699/000000?text=+ "dark pink #FF6699"
[pstblue]: https://placehold.it/15/9999FF/000000?text=+ "pastel blue #9999FF"
[ltgrey]: https://placehold.it/15/E0E0E0/000000?text=+ "light grey #E0E0E0 (MD color workaround https://github.com/github/markup/issues/369#issuecomment-654900693)"
